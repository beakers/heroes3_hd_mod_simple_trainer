#pragma once
enum class SkillState : char
{
	NoneState = 0,
	Basic = 1,
	Advanced = 2,
	Expert = 3
};

enum class SkillType : char
{
	Pathfinding = 0,
	Archery = 1,
	Logistics = 2,
	Scouting = 3,
	Diplomacy = 4,
	Navigation = 5,
	Leadership = 6,
	Wisdom = 7,
	Mysticism = 8,
	Luck = 9,
	Ballistics = 10,
	Eagle_Eye = 11,
	Necromancy = 12,
	Estates = 13,
	Fire_Magic = 14,
	Air_Magic = 15,
	Water_Magic = 16,
	Earth_Magic = 17,
	Learning = 18,
	Tactics = 19,
	Artillery = 20,
	Scholar = 21,
	Offence = 22,
	Armourer = 23,
	Intelligence = 24,
	Sorcery = 25,
	Resistance = 26,
	First_Aid = 27
};
struct Skill
{
	SkillType Type;
	SkillState State;
	int Slot;
	Skill(SkillType t, SkillState s, int sl)
	{
		Type = t;
		State = s;
		Slot = sl;
	}
};
