#pragma once
#include "Champion.h"
#include "Castle.h"

class Player
{
public:
	int GetID();
	int GetChampionsSize();
	int GetCastleSize();
	int SelectedCastle();
	int SelectedChampion();
	Champion* GetActveChampion();
	Castle* GetActiveCastle();
	int GetItem(int id);
	std::string GetPlayerName();
	int GetWood();
};