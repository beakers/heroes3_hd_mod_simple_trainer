#pragma once
struct Pos2D
{
	short X;
	short Y;
	char World;
	Pos2D() {}
	Pos2D(short x, short y, char w)
	{
		X = x;
		Y = y;
		World = w;
	}
};


struct Pos2DInt
{
	int X;
	int Y;
	char World;
	Pos2DInt(int x, int y, char w)
	{
		X = x;
		Y = y;
		World = w;
	}

};
