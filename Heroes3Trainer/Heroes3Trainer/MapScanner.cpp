#include "MapScanner.h"
#include "Game.h"
#include "Utils.h"

void MapData::Clear()
{
	IsInitialized = false;
	this->SubterraneanGates.clear();
}


MapScanner Map;

void MapScanner::AddPoint(MapObject* obj, Pos2D pos)
{
	switch (obj->GetType())
	{
	case ObjectType::Subterranean_Gate:
		if (obj->GetAvailableFlags() == 0x10)
			pos.World == 0 ? this->NormalWorld->SubterraneanGates.push_back(MapPoint(pos, obj)) : this->UnderWorld->SubterraneanGates.push_back(MapPoint(pos, obj));
		break;
	}
}

void MapScanner::Scan()
{
	this->NormalWorld->Clear();
	this->UnderWorld->Clear();

	for (int x = 0; x < Game.MapSize->X; ++x)
	{
		for (int y = 0; y < Game.MapSize->Y; ++y)
		{
			AddPoint(Game.EntityManager->GetObjectByCoord(Pos2D(x, y, 0)), Pos2D(x, y, 0));

			if (Game.EntityManager->IsUnderWorld())
				AddPoint(Game.EntityManager->GetObjectByCoord(Pos2D(x, y, 1)), Pos2D(x, y, 1));
		}
	}
}