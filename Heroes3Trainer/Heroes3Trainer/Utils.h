#pragma once
#include <Windows.h>
#include <time.h>
#include <iostream>
#include <vector>

class Utils
{
public:
	static void LogSystemInit()
	{
		AllocConsole();

		freopen("CONIN$", "r", stdin);
		freopen("CONOUT$", "w", stdout);
		freopen("CONOUT$", "w", stderr);

		char nBuffer[128];
		sprintf(nBuffer, "%s - Build from %s @ %s", "AutoAccept", __DATE__, __TIME__);
		SetConsoleTitleA(nBuffer);
	}

	static void* DetourFunction(BYTE *orig, BYTE *hook, int len)
	{
		DWORD dwProt = 0;
		BYTE *jmp = (BYTE*)malloc(len + 5);
		VirtualProtect(jmp, len + 5, PAGE_EXECUTE_READWRITE, &dwProt);
		VirtualProtect(orig, len, PAGE_EXECUTE_READWRITE, &dwProt);
		memcpy(jmp, orig, len);
		jmp += len;
		jmp[0] = 0xE9;
		*(DWORD*)(jmp + 1) = (DWORD)(orig + len - jmp) - 5;
		memset(orig, 0x90, len);
		orig[0] = 0xE9;
		*(DWORD*)(orig + 1) = (DWORD)(hook - orig) - 5;
		VirtualProtect(orig, len, dwProt, 0);
		return (jmp - len);
	}

	static void LogSystemClean()
	{
		FreeConsole();
	}

	static std::uint8_t* FindSignature(const char* szModule, const char* szSignature)
	{
		auto module = GetModuleHandle(szModule);
		static auto pattern_to_byte = [](const char* pattern) {
			auto bytes = std::vector<int>{};
			auto start = const_cast<char*>(pattern);
			auto end = const_cast<char*>(pattern) + strlen(pattern);

			for (auto current = start; current < end; ++current) {
				if (*current == '?') {
					++current;
					if (*current == '?')
						++current;
					bytes.push_back(-1);
				}
				else {
					bytes.push_back(strtoul(current, &current, 16));
				}
			}
			return bytes;
		};

		auto dosHeader = (PIMAGE_DOS_HEADER)module;
		auto ntHeaders = (PIMAGE_NT_HEADERS)((std::uint8_t*)module + dosHeader->e_lfanew);

		auto sizeOfImage = ntHeaders->OptionalHeader.SizeOfImage;
		auto patternBytes = pattern_to_byte(szSignature);
		auto scanBytes = reinterpret_cast<std::uint8_t*>(module);

		auto s = patternBytes.size();
		auto d = patternBytes.data();

		for (auto i = 0ul; i < sizeOfImage - s; ++i) {
			bool found = true;
			for (auto j = 0ul; j < s; ++j) {
				if (scanBytes[i + j] != d[j] && d[j] != -1) {
					found = false;
					break;
				}
			}
			if (found) {
				return &scanBytes[i];
			}
		}
		return nullptr;
	}

	static void Log(char* fmt, ...)
	{
		va_list va_alist;
		char logBuf[256] = { 0 };

		va_start(va_alist, fmt);
		_vsnprintf(logBuf + strlen(logBuf), sizeof(logBuf) - strlen(logBuf), fmt, va_alist);
		va_end(va_alist);

		if (logBuf[0] != '\0')
		{
			SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), (FOREGROUND_GREEN | FOREGROUND_INTENSITY));
			printf("[%s]", GetTimeSystem().c_str());
			SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), (FOREGROUND_RED | FOREGROUND_BLUE | FOREGROUND_GREEN));
			printf(": %s\n", logBuf);
		}
	}
	static std::string GetTimeSystem()
	{
		time_t current_time;
		struct tm *time_info;
		static char timeString[10];

		time(&current_time);
		time_info = localtime(&current_time);

		strftime(timeString, sizeof(timeString), "%I:%M %p", time_info);
		return timeString;
	}
};