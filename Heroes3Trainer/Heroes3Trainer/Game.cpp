#include "Game.h"
#include "Utils.h"
#include "MapScanner.h"
Globals Game;

void Globals::Init()
{
	Base = (DWORD)GetModuleHandle("Heroes3.exe");

	Map.NormalWorld = std::make_unique<MapData>();
	Map.UnderWorld = std::make_unique<MapData>();

	Utils::Log("Base: %p", Base);
	EntityManager = *reinterpret_cast<ObjectManager**>(Base + 0x299538);
	Utils::Log("EntityManager: %p", EntityManager);
	advManager = *reinterpret_cast<uintptr_t*>(Base + 0x2992B8);
	Utils::Log("advManager: %p", advManager);
	MapSize = reinterpret_cast<Pos2DInt*>(*reinterpret_cast<uintptr_t*>(Base + 0x6FB0));
	Utils::Log("MapSize: %p", MapSize);
}

Player* Globals::GetCurrentHero()
{
	return *reinterpret_cast<Player**>(Base + 0x29CCFC);
}

void Globals::RefreshGame()
{
	static auto RefreshGame3Fn = reinterpret_cast<int(__thiscall*)(uintptr_t, int)>(Utils::FindSignature("Heroes3.exe", "55 8B EC 66 8B 81"));
	RefreshGame3Fn(this->advManager, 0);
	static auto RefreshGame2Fn = reinterpret_cast<int(__thiscall*)(uintptr_t, int, int)>(Utils::FindSignature("Heroes3.exe", "55 8B EC 83 EC 08 56 68"));
	RefreshGame2Fn(this->advManager, 0, 0);
	static auto RefreshGameFn = reinterpret_cast<int(__thiscall*)(uintptr_t, int, int, int, int, int)>(Utils::FindSignature("Heroes3.exe", "55 8B EC 8B 45 18 8B 55 14 50 8B 45 10 52 8B 55 0C 50"));
	RefreshGameFn(this->advManager, 1, 1, 0, 0, 0);
}