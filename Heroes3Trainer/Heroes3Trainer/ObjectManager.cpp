#include "ObjectManager.h"
#include "Utils.h"

Champion* ObjectManager::GetChampion(int ID)
{
	return reinterpret_cast<Champion*>(this + 0x492 * ID + 0x21620);
}

Castle*  ObjectManager::GetCastle(int ID)
{
	ID *= 5;
	return reinterpret_cast<Castle*>(*reinterpret_cast<uintptr_t*>(this + 0x21614) + 72 * ID);
}

bool ObjectManager::IsUnderWorld()
{
	return *reinterpret_cast<char*>(this + 0x1FC48) + 1 > 1;
}

MapObject* ObjectManager::GetObjectByCoord(Pos2D Pos)
{
	static auto GetByCoord = reinterpret_cast<MapObject*(__thiscall*)(uintptr_t EntityManager, int a2, int a3, int a4)>(Utils::FindSignature("Heroes3.exe", "55 8B EC 8B 91 ? ? ? ? 56"));
	return GetByCoord(reinterpret_cast<uintptr_t>(this + 0x1FB70), Pos.X, Pos.Y, Pos.World);
}

Pos2D  ObjectManager::GetGrallPosition()
{
	return *reinterpret_cast<Pos2D*>(this + 0x1F690);
}