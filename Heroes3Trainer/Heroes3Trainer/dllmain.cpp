#include <Windows.h>
#include "Utils.h"
#include "MapScanner.h"
#include "Game.h"

class UtilsGame
{
public:
	static void ShowMap(int PlayerID)
	{
		static auto ShowMapFn = reinterpret_cast<void(__thiscall*)(ObjectManager *EntityList, int X_Start, int Y_start, int World, signed int PlayerID, int Radius, signed int unk3)>(Utils::FindSignature("Heroes3.exe", "55 8B EC 83 EC 38 8B 45 14 53 57"));
		ShowMapFn(Game.EntityManager, Game.MapSize->X / 2, Game.MapSize->Y / 2, 0, PlayerID, Game.MapSize->X, 0);
		if (Game.EntityManager->IsUnderWorld())
			ShowMapFn(Game.EntityManager, Game.MapSize->X / 2, Game.MapSize->Y / 2, 1, PlayerID, Game.MapSize->X, 0);
	}

	static void GetGrall(int PlayerID)
	{
		ShowMap(PlayerID);

		auto LocalPLayer = Game.GetCurrentHero();

		auto Champ = LocalPLayer->GetActveChampion();

		if (Champ)
		{
			Champ->SetDestinationPosition(Pos2DInt(Game.EntityManager->GetGrallPosition().X, Game.EntityManager->GetGrallPosition().Y, Game.EntityManager->GetGrallPosition().World));
			Game.RefreshGame();
			Champ->Refresh();
		}
	}
};

//Hook data
uintptr_t dw_OnNewRound;

typedef void(__thiscall* NewRoundFn)(ObjectManager* EntityList);
NewRoundFn O_OnNewRound;

char ToRestoreOnNewRound[6];

void __fastcall HkOnNewRound(ObjectManager* EntityList, void*)
{
	O_OnNewRound(EntityList);

	__asm pushad

	auto LocalPLayer = Game.GetCurrentHero();

	if (LocalPLayer->GetID() == Player_ID::Red)
	{
		auto Champ = LocalPLayer->GetActveChampion();

		if (Champ)
		{
			Champ->SetMovemntPoints(50000);
			Champ->SetMaxMovemntPoints(50000);
			Champ->SetMana(2000);
			Champ->GetActiveSpells()->ActiveAll();
			Champ->Refresh();
		}
	}

	__asm popad
}

HMODULE g_hModule;
void Init()
{
	Utils::LogSystemInit();

	Game.Init();

	Map.Scan();

	//Init hooks
	dw_OnNewRound = (uintptr_t)Utils::FindSignature("Heroes3.exe", "55 8B EC 83 EC 48 A1 ? ? ? ? 53 56");

	memcpy(ToRestoreOnNewRound, (void*)dw_OnNewRound, 6);

	O_OnNewRound = (NewRoundFn)Utils::DetourFunction((BYTE*)dw_OnNewRound, (BYTE*)HkOnNewRound, 6);


	while (true)
	{
		if (GetAsyncKeyState(VK_F8))
		{
			Utils::LogSystemClean();

			unsigned long old_protection = 0;

			VirtualProtect(reinterpret_cast<void*>(dw_OnNewRound), 6, PAGE_EXECUTE_READWRITE, &old_protection);

			memcpy(reinterpret_cast<void*>(dw_OnNewRound), ToRestoreOnNewRound, 6);

			VirtualProtect(reinterpret_cast<void*>(dw_OnNewRound), 6, old_protection, &old_protection);

			FreeLibraryAndExitThread(g_hModule, 0);
		}
		else if (GetAsyncKeyState(VK_F2))
		{
			auto LocalPLayer = Game.GetCurrentHero();

			auto Champ = LocalPLayer->GetActveChampion();

			if (Champ)
			{
				Champ->SetMovemntPoints(50000);
				Champ->SetMaxMovemntPoints(50000);
				Champ->SetMana(2000);
				Champ->GetActiveSpells()->ActiveAll();

				Champ->GetHeroStats()->Stats[0] = 99;
				Champ->GetHeroStats()->Stats[1] = 99;
				Champ->GetHeroStats()->Stats[2] = 99;
				Champ->GetHeroStats()->Stats[3] = 99;

				for (int i = 0; i < 70; i++)
					Champ->GetSpellbook()->IsLerned[i] = true;

				Champ->AddSkill(SkillType::Armourer, SkillState::Expert);
				Champ->AddSkill(SkillType::Earth_Magic, SkillState::Expert);
				Champ->AddSkill(SkillType::Air_Magic, SkillState::Expert);
				Champ->AddSkill(SkillType::Leadership, SkillState::Expert);
				Champ->AddSkill(SkillType::Wisdom, SkillState::Expert);
				Champ->AddSkill(SkillType::Offence, SkillState::Expert);

				Utils::Log("%p", Game.EntityManager->GetObjectByCoord(Champ->GetPosistion()));

				UtilsGame::GetGrall(LocalPLayer->GetID());

				Utils::Log("%p = %s - %i:%i", Champ, Champ->GetName().c_str(), Champ->GetPosistion().X, Champ->GetPosistion().Y);
			}
			auto Castle = LocalPLayer->GetActiveCastle();
			if (Castle)
			{
				Utils::Log("Castle: %p", Castle);
			}
			Utils::Log("%p - %s - %i", LocalPLayer, LocalPLayer->GetPlayerName().c_str(), LocalPLayer->GetItem(6));
		}
		Sleep(200);
	}
}

BOOL APIENTRY DllMain(HMODULE hModule,
	DWORD  ul_reason_for_call,
	LPVOID lpReserved
)
{
	switch (ul_reason_for_call)
	{
	case DLL_PROCESS_ATTACH:
		g_hModule = hModule;
		DisableThreadLibraryCalls(hModule);
		CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)Init, NULL, 0, NULL);
		break;
	case DLL_THREAD_ATTACH:
	case DLL_THREAD_DETACH:
	case DLL_PROCESS_DETACH:
		break;
	}
	return TRUE;
}

