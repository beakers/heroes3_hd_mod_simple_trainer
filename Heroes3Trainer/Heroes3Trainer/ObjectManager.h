#pragma once
#include "Pos2D.h"
#include <Windows.h>

class Champion;
class Castle;
class MapObject;

class ObjectManager
{
public:
	MapObject* GetObjectByCoord(Pos2D Pos);
	Champion* GetChampion(int ID);
	Castle* GetCastle(int ID);
	Pos2D GetGrallPosition();
	bool IsUnderWorld();
};

