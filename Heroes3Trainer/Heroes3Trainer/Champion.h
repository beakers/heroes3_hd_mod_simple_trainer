#pragma once
#include "Spellbook.h"
#include "ActiveChampionSpell.h"
#include "Skill.h"
#include "HeroStats.h"
#include "ArmySet.h"
#include "GameEnum.h"
#include "Pos2D.h"
#include <string>
#include <vector>

class Champion
{
public:

	ActiveChampionSpell* GetActiveSpells();
	std::string GetName();
	Spellbook* GetSpellbook();
	std::vector<ArtifactType> GetFreeArtifacts();
	void RemoveSkill(SkillType Type);
	void AddSkill(SkillType Type, SkillState State);
	std::vector<Skill> GetSkillSet();
	int GetSkillSetCount();
	int GetLucky();
	int GetMorale();
	HeroStats* GetHeroStats();
	short GetMana();
	void SetMana(short mana);
	int GetID();
	ArmySet* GetArmy();
	Pos2D GetPosistion();
	void SetPosistion(Pos2D pos);
	Pos2DInt GetDestinationPosition();
	void SetDestinationPosition(Pos2DInt point);
	unsigned int GetMovementPoints();
	void SetMovemntPoints(unsigned int Value);
	void SetMaxMovemntPoints(unsigned int Value);
	unsigned int GetMaxMovementPoints();
	void Refresh();
};