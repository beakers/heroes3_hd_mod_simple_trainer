#pragma once
enum class ObjectType : int
{
	Empty_Ground = 0,
	Subterranean_Gate = 103
};

class MapObject
{
public:
	int GetAvailableFlags();
	ObjectType GetType();
};