#pragma once
#include <Windows.h>
#include "ObjectManager.h"
#include "Player.h"

class Globals
{
public:
	void Init();
	Player* GetCurrentHero();
	void RefreshGame();
	uintptr_t Base;
	uintptr_t advManager;
	ObjectManager* EntityManager;
	Pos2DInt* MapSize;
};

extern Globals Game;

