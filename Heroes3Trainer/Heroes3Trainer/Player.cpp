#include "Player.h"
#include "Game.h"

int Player::GetID()
{
	return *reinterpret_cast<char*>(this + 0x0);
}

int Player::GetChampionsSize()
{
	return *reinterpret_cast<char*>(this + 0x1);
}

int Player::GetCastleSize()
{
	return *reinterpret_cast<char*>(this + 0x3E);
}

int Player::SelectedCastle()
{
	return *reinterpret_cast<char*>(this + 0x3F);
}

int Player::SelectedChampion()
{
	return *reinterpret_cast<int*>(this + 0x4);
}

Champion* Player::GetActveChampion()
{
	auto SelectedID = SelectedChampion();
	return SelectedID != -1 ? Game.EntityManager->GetChampion(SelectedID) : nullptr;
}

Castle* Player::GetActiveCastle()
{
	auto SelectedID = SelectedCastle();
	return SelectedID != -1 ? Game.EntityManager->GetCastle(SelectedID) : nullptr;
}

int Player::GetItem(int id)
{
	return *reinterpret_cast<int*>(this + 0x9C + id * 0x4);
}

std::string Player::GetPlayerName()
{
	return std::string(reinterpret_cast<const char*>(this + 0xCC));
}

int Player::GetWood()
{
	return GetItem(0);
}