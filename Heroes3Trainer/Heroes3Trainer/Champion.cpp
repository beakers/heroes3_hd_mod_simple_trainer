#include "Champion.h"
#include "Utils.h"
#include "Game.h"

ActiveChampionSpell* Champion::GetActiveSpells()
{
	return reinterpret_cast<ActiveChampionSpell*>(this + 0x10E);
}

std::string Champion::GetName()
{
	return std::string(reinterpret_cast<const char*>(this + 0x23));
}

Spellbook* Champion::GetSpellbook()
{
	return reinterpret_cast<Spellbook*>(this + 0x430);
}

std::vector<ArtifactType> Champion::GetFreeArtifacts()
{
	std::vector<ArtifactType> Artifacts;
	for (int i = reinterpret_cast<uintptr_t>(this + 0x1D4); i <= reinterpret_cast<uintptr_t>(this + 0x1FC); i += 0x8)
	{
		ArtifactType current = *reinterpret_cast<ArtifactType*>(i);
		if (current != ArtifactType::None_NULL)
			Artifacts.push_back(current);
	}
	return Artifacts;
}

void Champion::RemoveSkill(SkillType Type)
{
	auto Current = GetSkillSet();
	for (auto Skill : Current)
	{
		if (Skill.Type == Type)
		{
			int CountToMove = Skill.Slot;
			*reinterpret_cast<char*>(this + (char)Type + 0xE5) = 0;
			*reinterpret_cast<SkillState*>(this + (char)Type + 0xC9) = SkillState::NoneState;
			*reinterpret_cast<char*>(this + 0x101) = Current.size() - 1;
			if (CountToMove - Current.size() == 0)
				return;
			else
			{
				for (auto NextSkill : Current)
				{
					if (NextSkill.Slot != Skill.Slot && NextSkill.Slot > CountToMove)
						*reinterpret_cast<char*>(this + (char)NextSkill.Type + 0xE5) = NextSkill.Slot - 1;
				}
				return;
			}
		}
	}
}

void Champion::AddSkill(SkillType Type, SkillState State)
{
	auto Current = GetSkillSet();
	for (auto Skill : Current)
	{
		if (Skill.Type == Type)
		{
			*reinterpret_cast<SkillState*>(this + (char)Skill.Type + 0xC9) = State;
			return;
		}
	}
	if (Current.size() >= 8)
		return;

	char FirstFreeSlot = Current.size() + 1;
	*reinterpret_cast<char*>(this + (char)Type + 0xE5) = FirstFreeSlot;
	*reinterpret_cast<SkillState*>(this + (char)Type + 0xC9) = State;
	*reinterpret_cast<char*>(this + 0x101) = FirstFreeSlot;
}

std::vector<Skill> Champion::GetSkillSet()
{
	std::vector<Skill> Skills;
	int Slot = 1;

	while (Slot <= GetSkillSetCount())
	{
		int Type = 0;
		while (*reinterpret_cast<char*>(this + Type + 0xE5) != Slot)
		{
			if (++Type >= 28)
			{
				Type = -1;
				break;
			}
		}
		if (Type != -1)
		{
			auto State = *reinterpret_cast<SkillState*>(this + Type + 0xC9);
			auto SkillT = (SkillType)Type;
			Skills.push_back(Skill(SkillT, State, Slot));
		}
		++Slot;
	}

	return Skills;
}

int Champion::GetSkillSetCount()
{
	return *reinterpret_cast<char*>(this + 0x101);
}

int Champion::GetLucky()
{
	return *reinterpret_cast<char*>(this + 0x11B);
}

int Champion::GetMorale()
{
	return *reinterpret_cast<char*>(this + 0x11A);
}

HeroStats* Champion::GetHeroStats()
{
	return reinterpret_cast<HeroStats*>(this + 0x476);
}

short Champion::GetMana()
{
	return *reinterpret_cast<short*>(this + 0x18);
}

void Champion::SetMana(short mana)
{
	*reinterpret_cast<short*>(this + 0x18) = mana;
}

int Champion::GetID()
{
	return *reinterpret_cast<int*>(this + 0x1A);
}

ArmySet* Champion::GetArmy()
{
	return reinterpret_cast<ArmySet*>(this + 0x91);
}

Pos2D Champion::GetPosistion()
{
	return *reinterpret_cast<Pos2D*>(this + 0x0);
}

void Champion::SetPosistion(Pos2D pos)
{
	*reinterpret_cast<Pos2D*>(this + 0x0) = pos;
}

Pos2DInt Champion::GetDestinationPosition()
{
	return *reinterpret_cast<Pos2DInt*>(this + 0x35);
}

void Champion::SetDestinationPosition(Pos2DInt point)
{
	*reinterpret_cast<Pos2DInt*>(this + 0x35) = point;
}

unsigned int Champion::GetMovementPoints()
{
	return *reinterpret_cast<unsigned int*>(this + 0x4D);
}

void Champion::SetMovemntPoints(unsigned int Value)
{
	*reinterpret_cast<unsigned int*>(this + 0x4D) = Value;
}

void Champion::SetMaxMovemntPoints(unsigned int Value)
{
	*reinterpret_cast<unsigned int*>(this + 0x49) = Value;
}

unsigned int Champion::GetMaxMovementPoints()
{
	return *reinterpret_cast<unsigned int*>(this + 0x49);
}

void Champion::Refresh()
{
	static auto RefreshChampionFn = reinterpret_cast<int(__thiscall*)(Champion*, int)>(Utils::FindSignature("Heroes3.exe", "55 8B EC 83 EC 14 8B 45 08 56"));
	RefreshChampionFn(this, 3);
	*reinterpret_cast<int*>(Game.advManager + 0x50) = 0;
}