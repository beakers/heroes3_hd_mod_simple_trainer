#pragma once
class ActiveChampionSpell
{
public:
	int Hide;
	int Fly;
	int WaterWalks;

	void ActiveAll()
	{
		Hide = 3;
		Fly = 3;
		WaterWalks = 0;
	}

	void DeactiveAll()
	{
		Hide = -1;
		Fly = -1;
		WaterWalks = -1;
	}
};