#pragma once
#include "MapObject.h"
#include <memory>
#include <vector>
#include "Pos2D.h"

class MapPoint
{
public:
	Pos2D Pos;
	MapObject* Object = nullptr;
	MapPoint(Pos2D pos, MapObject* obj)
	{
		Pos = pos;
		Object = obj;
	}
};

class MapData
{
public:
	std::vector<MapPoint> SubterraneanGates;
	bool IsInitialized = false;
	void Clear();
	MapData() {}
};

class MapScanner
{
public:
	std::unique_ptr<MapData> NormalWorld;
	std::unique_ptr<MapData> UnderWorld;
	void Scan();
	void AddPoint(MapObject* obj, Pos2D pos);
};

extern MapScanner Map;