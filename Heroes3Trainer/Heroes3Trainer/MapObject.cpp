#include "MapObject.h"

ObjectType MapObject::GetType()
{
	return *reinterpret_cast<ObjectType*>(this + 0x1E);
}
int MapObject::GetAvailableFlags()
{
	return *reinterpret_cast<char*>(this + 0xD);
}